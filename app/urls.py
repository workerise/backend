from django.urls import include, path
from rest_framework import routers

# User Management Views
from app.user_management.views.company import CompanyViewSet, initialize_company
from app.user_management.views.invited_user import (
    InvitedUserViewset,
    accept_invite,
    invite_user,
)
from app.user_management.views.user import (
    UserViewSet,
    login,
    user_data,
    change_password,
)

# Timeline Views
from app.timeline import views as timeline_views

router = routers.DefaultRouter()

# User Management Model Viewsets
router.register(r"companies", CompanyViewSet, basename="companies")
router.register(r"users", UserViewSet, basename="users")
router.register(r"invited-users", InvitedUserViewset, basename="invited-users")
# Timeline Model Viewsets
router.register(r"tasks", timeline_views.TaskViewSet, basename="tasks")

urlpatterns = [
    # Automatic URL routing.
    path("", include(router.urls)),
    # User Managment Custom Endpoints
    path("initialize-company/", initialize_company),
    path("login/", login),
    path("user-data/", user_data),
    path("change-password/", change_password),
    path("invite-user/", invite_user),
    path("accept-invite/", accept_invite),
    # Authenticate Browsable API
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
]
