from rest_framework import viewsets, permissions, mixins
from rest_framework.authentication import TokenAuthentication

from app.user_management.models import UserSettings

from app.timeline.models import Task
from app.timeline.serializers import (
    TaskSerializer,
)


class TaskViewSet(
    mixins.UpdateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    """
    API endpoint that allows tasks to be viewed or edited.
    """

    serializer_class = TaskSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        # Only return own company
        req_user_settings = UserSettings.objects.get(user=self.request.user)
        req_user_company = req_user_settings.company
        return Task.objects.filter(company=req_user_company)
