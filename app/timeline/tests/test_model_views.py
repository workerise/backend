from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from app.user_management.models import Company, UserSettings
from django.contrib.auth.models import User

from app.timeline.models import Task


class TestTasksViewset(APITestCase):
    def setUp(self):
        # Creates log in user and auth
        self.user = User.objects.create(
            username="default", email="hewwo@hewwo.org", password="1234AbcD!"
        )
        self.token = Token.objects.create(user=self.user)

        # Create user company
        self.company = Company.objects.create(name="Kramerica Industries")
        self.user_settings = UserSettings.objects.create(
            user=self.user, company=self.company
        )
        # Tasks for user company
        self.task1 = Task.objects.create(key="task1", company=self.company)
        self.task2 = Task.objects.create(key="task2", company=self.company)

        # Creates other company
        self.different_company = Company.objects.create(name="Google Inc.")
        # Tasks for other company
        self.task3 = Task.objects.create(key="task3", company=self.different_company)
        self.task4 = Task.objects.create(key="task4", company=self.different_company)

        # API client
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

    # Returns all tasks for the user's company
    def test_succeeds_get_task_list(self):
        response = self.client.get("/tasks/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    # User can not create a task
    def test_fail_create_task(self):
        # In the future, maybe users can create custom tasks
        response = self.client.post(
            "/tasks/",
            {
                "key": "test-key",
                "company": self.company.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    # User can not delete a task
    def test_fail_delete_task(self):
        task_url = "/tasks/{}/".format(self.task1.id)
        response = self.client.delete(
            task_url,
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    # User successfully marks a task as complete in their own company
    def test_succeeds_edit_task(self):
        task_url = "/tasks/{}/".format(self.task1.id)
        self.assertEqual(self.task1.completed, False)
        response = self.client.patch(task_url, {"completed": True})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        reloaded_task_1 = Task.objects.get(id=self.task1.id)
        self.assertEqual(reloaded_task_1.completed, True)

    # User can not edits tasks in another company
    def test_fail_edit_task_other_company(self):
        task_url = "/tasks/{}/".format(self.task3.id)
        self.assertEqual(self.task3.completed, False)
        response = self.client.patch(task_url, {"completed": True})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        reloaded_task_3 = Task.objects.get(id=self.task3.id)
        self.assertEqual(self.task3.completed, False)