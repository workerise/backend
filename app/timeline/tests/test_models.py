import datetime
from django.test import TestCase

from app.timeline.models import Task
from app.user_management.models import Company

from app.timeline.config.tasks import tasks_definitions


class TaskListTests(TestCase):
    def setUp(self):
        self.company = Company.objects.create(name="Kramerica Industries")
        self.task_list = Task.generate_tasklist(self.company)

    def test_succeeds_generate_task_list(self):

        # Tasks generated
        self.assertEqual(Task.objects.count(), len(tasks_definitions))

        # All tasks have company added
        for task in Task.objects.filter(company=self.company):
            self.assertEqual(task.company, self.company)

    def test_succeeds_sets_depends_on(self):
        self.assertTrue(
            Task.objects.get(key="invite-election-board-election", company=self.company)
            in Task.objects.get(
                key="election-board-election", company=self.company
            ).depends_on.all()
        )


class TaskDueDateTests(TestCase):
    def setUp(self):
        # Create task list
        self.company = Company.objects.create(name="Kramerica Industries")
        self.task_list = Task.generate_tasklist(self.company)

        # Set a required task as completed
        self.completed_date = datetime.date(2020, 1, 7)

        self.board_election_task = Task.objects.get(
            key="election-board-election", company=self.company
        )

        self.board_election_task.completed = True
        self.board_election_task.completed_date = self.completed_date
        self.board_election_task.save()

        self.council_election_task = Task.objects.get(
            key="council-election", company=self.company
        )

    def test_succeeds_complete_sets_due_date(self):
        # Expected due date is 7 days after the
        # completion date of the election board election
        expected_due_date = datetime.date(2020, 1, 14)
        self.assertEqual(self.council_election_task.due_date, expected_due_date)

    def test_succeeds_change_complete_date(self):
        # Change completed date one day back
        new_date = datetime.date(2020, 1, 6)
        self.board_election_task.completed_date = new_date
        self.board_election_task.save()

        self.assertEqual(
            Task.objects.get(key="council-election", company=self.company).due_date,
            datetime.date(2020, 1, 13),
        )

    def test_succeeds_incomplete_undoes_due_date(self):
        # Change completed date one day back
        self.board_election_task.completed = False
        self.board_election_task.completed_date = None
        self.board_election_task.save()

        self.assertEqual(
            Task.objects.get(key="council-election", company=self.company).due_date,
            None,
        )