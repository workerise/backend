from __future__ import unicode_literals

import datetime
import functools

from django.conf import settings
from django.db import models
from django.utils import timezone

from app.user_management.models import Company

from .config.tasks import tasks_definitions


class Task(models.Model):
    # Static Fields
    key = models.CharField(max_length=140, editable=False)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, editable=False)
    required = models.BooleanField(default=False, editable=False)
    depends_on = models.ManyToManyField(to="Task")
    # Dynamic Fields
    completed = models.BooleanField(default=False)
    completed_date = models.DateField(blank=True, null=True)
    due_date = models.DateField(blank=True, null=True)
    scheduled_time = models.DateTimeField(blank=True, null=True)

    def save(self, **kwargs):
        # If any due dates should be set as a result of this action completing
        self.set_relative_due_dates()
        super(Task, self).save()

    def set_relative_due_dates(self):
        """
        If the user sets the task as completed and sets the completed date
        then update all the tasks that depend on this task.
        """
        # If user marks the task as complete and provides completed date
        if self.completed and self.completed_date:
            # Get task definition for the saved task
            definition = tasks_definitions[self.key]
            # Get task objects that get their dates set
            for task_with_due_date in definition.sets_due_date_for:
                key, days = task_with_due_date
                task_to_update = Task.objects.get(key=key, company=self.company)
                new_due_date = self.completed_date + datetime.timedelta(days=days)
                task_to_update.due_date = new_due_date
                task_to_update.save()
        # If user marks the task as incomplete
        elif not self.completed and not self.completed_date and not self._state.adding:
            definition = tasks_definitions[self.key]
            for task_with_due_date in definition.sets_due_date_for:
                key, days = task_with_due_date
                task_to_update = Task.objects.get(key=key, company=self.company)
                # If has due date set
                if task_to_update.due_date is not None:
                    # Remove it
                    task_to_update.due_date = None
                    task_to_update.save()

    @classmethod
    def generate_tasklist(cls, company):
        """
        Generates task objects from the tasks in ./tasks.py
        """
        for task_key, task in tasks_definitions.items():
            new_task = Task.objects.create(
                key=task.key, company=company, required=task.required
            )
            # If dependencies
            if len(task.depends_on) > 0:
                for dependency in task.depends_on:
                    # Get objects for each dependency
                    dep_to_add = Task.objects.get(key=dependency, company=company)
                    # Add to many to many field
                    new_task.depends_on.add(dep_to_add)
