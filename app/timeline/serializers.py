from rest_framework import serializers
from django.contrib.auth.models import Group
from .models import Task


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = [
            "id",
            "key",
            "required",
            "depends_on",
            "completed",
            "completed_date",
            "due_date",
            "scheduled_time",
        ]
