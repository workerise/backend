from abc import ABC
from dataclasses import dataclass, field
from typing import List, Tuple, Any


@dataclass
class Task(ABC):
    key: str  # Key is unique for each task
    # Placeholders
    # betrvg_path: str  # Need to understand how the BetrVG is modelled for this path to work
    # subtask_of: str  # Maybe we want some tasks to relate to another task?

    required: bool = False
    depends_on: List[str] = field(default_factory=list)
    sets_due_date_for: Tuple[Tuple[str, int], ...] = field(default_factory=tuple)


InviteColleagues = Task(key="invite-colleagues")

ContactUnion = Task(key="contact-union")

# Required
InviteElectionBoardElection = Task(key="invite-election-board-election", required=True)

ElectionBoardElection = Task(
    key="election-board-election",
    required=True,
    depends_on=["invite-election-board-election"],
    sets_due_date_for=(("council-election", 7), ("invite-council-election", 6)),
)

InviteElection = Task(
    key="invite-council-election",
    required=True,
    depends_on=["election-board-election"],
)

CouncilElection = Task(
    key="council-election",
    required=True,
    depends_on=["invite-council-election", "election-board-election"],
)

tasks_definitions = {
    InviteElectionBoardElection.key: InviteElectionBoardElection,
    ElectionBoardElection.key: ElectionBoardElection,
    InviteElection.key: InviteElection,
    CouncilElection.key: CouncilElection,
}
