import unittest

from app.timeline.config.works_council_size_map import WorksCouncilSizeMap

class TestWorksCouncilSizeMap(unittest.TestCase):
    def setUp(self):
        self.map = WorksCouncilSizeMap()
    
    def test_null_employees(self):
        self.assertEqual(None, self.map.get_number_of_works_council_members(None))
        
    def test_less_than_5_employees(self):
        self.assertEqual(0, self.map.get_number_of_works_council_members(4))

    def test_5_employees(self):
        self.assertEqual(1, self.map.get_number_of_works_council_members(5))

    def test_less_than_20_employees(self):
        self.assertEqual(1, self.map.get_number_of_works_council_members(14))

    def test_20_employees(self):
        self.assertEqual(1, self.map.get_number_of_works_council_members(20))

    def test_21_employees(self):
        self.assertEqual(3, self.map.get_number_of_works_council_members(21))

    def test_less_than_50_employees(self):
        self.assertEqual(3, self.map.get_number_of_works_council_members(42))

    def test_50_employees(self):
        self.assertEqual(3, self.map.get_number_of_works_council_members(50))

    def test_51_employees(self):
        self.assertEqual(5, self.map.get_number_of_works_council_members(51))

    def test_less_than_100_employees(self):
        self.assertEqual(5, self.map.get_number_of_works_council_members(77))

    def test_100_employees(self):
        self.assertEqual(5, self.map.get_number_of_works_council_members(100))

    def test_101_employees(self):
        self.assertEqual(7, self.map.get_number_of_works_council_members(101))

    def test_less_than_200_employees(self):
        self.assertEqual(7, self.map.get_number_of_works_council_members(123))

    def test_200_employees(self):
        self.assertEqual(7, self.map.get_number_of_works_council_members(200))

    def test_201_employees(self):
        self.assertEqual(9, self.map.get_number_of_works_council_members(201))

    def test_less_than_400_employees(self):
        self.assertEqual(9, self.map.get_number_of_works_council_members(345))

    def test_400_employees(self):
        self.assertEqual(9, self.map.get_number_of_works_council_members(400))

    def test_401_employees(self):
        self.assertEqual(11, self.map.get_number_of_works_council_members(401))

    def test_less_than_700_employees(self):
        self.assertEqual(11, self.map.get_number_of_works_council_members(555))

    def test_700_employees(self):
        self.assertEqual(11, self.map.get_number_of_works_council_members(700))

    def test_701_employees(self):
        self.assertEqual(13, self.map.get_number_of_works_council_members(701))

    def test_less_than_1000_employees(self):
        self.assertEqual(13, self.map.get_number_of_works_council_members(789))

    def test_1000_employees(self):
        self.assertEqual(13, self.map.get_number_of_works_council_members(1000))

    def test_1001_employees(self):
        self.assertEqual(15, self.map.get_number_of_works_council_members(1001))

    def test_less_than_1500_employees(self):
        self.assertEqual(15, self.map.get_number_of_works_council_members(1234))

    def test_1500_employees(self):
        self.assertEqual(15, self.map.get_number_of_works_council_members(1500))

    def test_1501_employees(self):
        self.assertEqual(17, self.map.get_number_of_works_council_members(1501))

    def test_less_than_2000_employees(self):
        self.assertEqual(17, self.map.get_number_of_works_council_members(1765))

    def test_2000_employees(self):
        self.assertEqual(17, self.map.get_number_of_works_council_members(2000))

    def test_2001_employees(self):
        self.assertEqual(19, self.map.get_number_of_works_council_members(2001))

    def test_less_than_2500_employees(self):
        self.assertEqual(19, self.map.get_number_of_works_council_members(2222))

    def test_2500_employees(self):
        self.assertEqual(19, self.map.get_number_of_works_council_members(2500))

    def test_2501_employees(self):
        self.assertEqual(21, self.map.get_number_of_works_council_members(2501))

    def test_less_than_3000_employees(self):
        self.assertEqual(21, self.map.get_number_of_works_council_members(2765))

    def test_3000_employees(self):
        self.assertEqual(21, self.map.get_number_of_works_council_members(3000))

    def test_3001_employees(self):
        self.assertEqual(23, self.map.get_number_of_works_council_members(3001))

    def test_less_than_3500_employees(self):
        self.assertEqual(23, self.map.get_number_of_works_council_members(3232))

    def test_3500_employees(self):
        self.assertEqual(23, self.map.get_number_of_works_council_members(3500))

    def test_less_than_4000_employees(self):
        self.assertEqual(25, self.map.get_number_of_works_council_members(3598))

    def test_4000_employees(self):
        self.assertEqual(25, self.map.get_number_of_works_council_members(4000))

    def test_4001_employees(self):
        self.assertEqual(27, self.map.get_number_of_works_council_members(4001))

    def test_less_than_4500_employees(self):
        self.assertEqual(27, self.map.get_number_of_works_council_members(4262))

    def test_4500_employees(self):
        self.assertEqual(27, self.map.get_number_of_works_council_members(4500))

    def test_4501_employees(self):
        self.assertEqual(29, self.map.get_number_of_works_council_members(4501))

    def test_less_than_5000_employees(self):
        self.assertEqual(29, self.map.get_number_of_works_council_members(4876))

    def test_5000_employees(self):
        self.assertEqual(29, self.map.get_number_of_works_council_members(5000))

    def test_5001_employees(self):
        self.assertEqual(31, self.map.get_number_of_works_council_members(5001))

    def test_less_than_6000_employees(self):
        self.assertEqual(31, self.map.get_number_of_works_council_members(5555))

    def test_6000_employees(self):
        self.assertEqual(31, self.map.get_number_of_works_council_members(6000))

    def test_6001_employees(self):
        self.assertEqual(33, self.map.get_number_of_works_council_members(6001))

    def test_less_than_7000_employees(self):
        self.assertEqual(33, self.map.get_number_of_works_council_members(6655))

    def test_7000_employees(self):
        self.assertEqual(33, self.map.get_number_of_works_council_members(7000))

    def test_7001_employees(self):
        self.assertEqual(35, self.map.get_number_of_works_council_members(7001))

    def test_less_than_9000_employees(self):
        self.assertEqual(35, self.map.get_number_of_works_council_members(8000))

    def test_9000_employees(self):
        self.assertEqual(35, self.map.get_number_of_works_council_members(9000))

    def test_more_than_9000_employees(self):
        self.assertEqual(None, self.map.get_number_of_works_council_members(9999))

