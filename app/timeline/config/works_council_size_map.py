class WorksCouncilSizeMap():
    def get_number_of_works_council_members(self, number_of_employees : int):
        if number_of_employees is None:
            return None
        for category in num_works_council_members:
            min = category["min"]
            max = category["max"]
            if min > number_of_employees:
                return 0
            elif min <= number_of_employees and max >= number_of_employees:
                return category["num_members"]
        return None

num_works_council_members = [
    {"min": 5, "max": 20, "num_members": 1},
    {"min": 21, "max": 50, "num_members": 3},
    {"min": 51, "max": 100, "num_members": 5},
    {"min": 101, "max": 200, "num_members": 7},
    {"min": 201, "max": 400, "num_members": 9},
    {"min": 401, "max": 700, "num_members": 11},
    {"min": 701, "max": 1000, "num_members": 13},
    {"min": 1001, "max": 1500, "num_members": 15},
    {"min": 1501, "max": 2000, "num_members": 17},
    {"min": 2001, "max": 2500, "num_members": 19},
    {"min": 2501, "max": 3000, "num_members": 21},
    {"min": 3001, "max": 3500, "num_members": 23},
    {"min": 3501, "max": 4000, "num_members": 25},
    {"min": 4001, "max": 4500, "num_members": 27},
    {"min": 4501, "max": 5000, "num_members": 29},
    {"min": 5001, "max": 6000, "num_members": 31},
    {"min": 6001, "max": 7000, "num_members": 33},
    {"min": 7001, "max": 9000, "num_members": 35},
]