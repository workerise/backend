from rest_framework import serializers
from django.contrib.auth.models import User, Group
from .models import Company, UserSettings, InvitedUser


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ["id", "name", "number_of_employees"]
        read_only_fields = ["number_of_works_council_members"]


class InvitedUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvitedUser
        fields = ["id", "email", "company"]


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "username", "email"]


class UserSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserSettings
        fields = ["id", "user", "company"]
