from django.db.utils import IntegrityError

from rest_framework import (
    viewsets,
    permissions,
    authentication,
    serializers,
    status,
    mixins,
)
from rest_framework.response import Response
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from app.timeline.models import Task
from app.user_management.models import Company, UserSettings
from app.user_management.serializers import CompanySerializer, UserSerializer


class CompanyViewSet(
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet
):
    """
    Allows the user to retrieve or edit their own company.

    It can not be created or deleted.

    Company models are only created by using the endpoint `/initialize-company/`,
    which also creates a new user with that company.
    """

    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        # Only return own company
        req_user_settings = UserSettings.objects.get(user=self.request.user)
        req_user_company = req_user_settings.company
        return Company.objects.filter(id=req_user_company.id)


@api_view(["POST"])
def initialize_company(request):
    # Extract data
    user_data = {
        "username": request.data.get("username"),
        "email": request.data.get("email"),
        "password": request.data.get("password"),
    }
    company_data = {
        "name": request.data.get("company_name"),
        "number_of_employees": request.data.get("company_number_of_employees"),
        }
    # Validate
    company_serializer = CompanySerializer(data=company_data)
    if not company_serializer.is_valid():
        raise serializers.ValidationError(detail=company_serializer.errors)

    user_serializer = UserSerializer(data=request.data)
    if not user_serializer.is_valid():
        raise serializers.ValidationError(detail=user_serializer.errors)
    # Create Objects
    try:
        # Create Company
        company = company_serializer.create(
            validated_data=company_serializer.validated_data
        )
        # Create User
        user = user_serializer.create(validated_data=user_serializer.validated_data)
        user.set_password(user_data["password"])
        user.save()
        # Create UserSettings
        user_settings = UserSettings.objects.create(user=user, company=company)
        # Initialise tasks
        Task.generate_tasklist(company)
    # In case of error saving data
    except IntegrityError as err:
        # Returns a 500 error
        raise APIException(detail=err)
    # Return response if no errors
    return Response(
        {
            "user": {
                "id": user.id,
                "username": user_serializer.validated_data["username"],
            },
        },
        status=status.HTTP_201_CREATED,
    )