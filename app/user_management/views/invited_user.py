from decouple import config

from rest_framework import views, viewsets, permissions, status, serializers, mixins
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import APIException
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

from app.user_management.models import InvitedUser, Company, UserSettings
from app.user_management.serializers import (
    InvitedUserSerializer,
    UserSerializer,
    UserSettingsSerializer,
)
from app.user_management.emails.invite_user_email import build_invite_user_email


class InvitedUserViewset(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """
    Allows the viewer to see a list of Invited Users to their company.
    They can also delete any Invited User invited to their company.

    Invited User models are only created through the `/invite-user/` endpoint

    This endpoint allows GET requests for retrieve() without auth and returns
    the company name

    TODO: GET requests for list() should use authentication. This must be tested!
    """

    serializer_class = InvitedUserSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        # Only return invited users from own company
        req_user_settings = UserSettings.objects.get(user=self.request.user)
        req_user_company = req_user_settings.company
        return InvitedUser.objects.filter(company=req_user_company)

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        invited_user = InvitedUser.objects.get(pk=pk)
        serializer = self.get_serializer(invited_user)
        return Response({**serializer.data, "company_name": invited_user.company.name})


# Invite User
@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def invite_user(request):
    user_settings = UserSettings.objects.get(user=request.user)
    company = user_settings.company
    serializer = InvitedUserSerializer(
        data={"email": request.data["email"], "company": company.id}
    )
    if not serializer.is_valid():
        raise serializers.ValidationError(detail=serializer.errors)
    # Create Invited User
    invited_user = serializer.create(
        validated_data={"email": serializer.data["email"], "company": company}
    )

    # Send Email to invited user
    message = Mail(
        from_email="river@workerise.org",
        to_emails=serializer.data["email"],
        subject="Invitation to Workerise / Einladung zu Workerise",
        html_content=build_invite_user_email(invited_user.id),
    )
    try:
        SENDGRID_API_KEY = config("SENDGRID_API_KEY", "default_api_key")
        sg = SendGridAPIClient(api_key=SENDGRID_API_KEY)
        response = sg.send(message)
    except Exception as e:
        # Error sending email
        print(e)
        return Response(
            {"email_service": ["Failed to send email"]},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    return Response({"id": invited_user.id}, status=status.HTTP_201_CREATED)


# Accept Invite
@api_view(["POST"])
def accept_invite(request):
    try:
        invited_user = InvitedUser.objects.get(id=request.data.get("id"))
    except InvitedUser.DoesNotExist as error:
        raise APIException(detail=error)
    user_data = {
        "username": request.data.get("username"),
        "email": invited_user.email,
        "password": request.data.get("password"),
    }
    serializer = UserSerializer(data=user_data)
    # Validate
    if not serializer.is_valid():
        raise serializers.ValidationError(detail=serializer.errors)
    # Create User and settings
    user = serializer.create(validated_data=serializer.validated_data)
    user.set_password(user_data["password"])
    user.save()
    # TODO Use UserSettings Serializer
    user_settings = UserSettings.objects.create(user=user, company=invited_user.company)
    # Remove invited user
    invited_user.delete()
    return Response({"id": user.id}, status=status.HTTP_201_CREATED)
