from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from unittest import mock
from app.user_management.models import Company, InvitedUser, UserSettings
from django.contrib.auth.models import User

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient


class TestInvitedUsersViewset(APITestCase):
    def setUp(self):
        # Creates log in user
        self.user = User.objects.create(username="default", password="1234AbcD!")
        self.company = Company.objects.create(name="Kramerica Industries")
        self.user_settings = UserSettings.objects.create(
            user=self.user, company=self.company
        )
        self.token = Token.objects.create(user=self.user)

        # Creates colleague invite
        self.colleague = InvitedUser.objects.create(
            email="florentin@will.de", company=self.company
        )

        # Creates other user with different company
        self.different_company = Company.objects.create(name="Google Inc.")
        self.stranger = InvitedUser.objects.create(
            email="jorin@vogel.de", company=self.different_company
        )

        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

    # List
    def test_succeeds_list_own_company(self):
        response = self.client.get("/invited-users/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    # Detail
    def test_succeeds_detail(self):
        # Doesn't require a token
        unauth_client = APIClient()
        response = unauth_client.get("/invited-users/{}/".format(self.colleague.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # Edit
    def test_fails_edit(self):
        response = self.client.patch(
            "/invited-users/{}/".format(self.colleague.id), {"email": "hewwo@email.com"}
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    # Delete
    def test_succeeds_delete_own_company(self):
        self.assertEqual(2, InvitedUser.objects.count())
        response = self.client.delete("/invited-users/{}/".format(self.colleague.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(1, InvitedUser.objects.count())

    def test_fails_delete_other_company(self):
        self.assertEqual(2, InvitedUser.objects.count())
        response = self.client.delete("/invited-users/{}/".format(self.stranger.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(2, InvitedUser.objects.count())


class InviteUserTests(APITestCase):
    def setUp(self):
        # Creates log in user to make the invitation
        self.user = User.objects.create(username="default", password="1234AbcD!")
        self.company = Company.objects.create(name="Kramerica Industries")
        self.user_settings = UserSettings.objects.create(
            user=self.user, company=self.company
        )
        self.token = Token.objects.create(user=self.user)

        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

    @mock.patch("app.user_management.views.invited_user.SendGridAPIClient.send")
    @mock.patch("app.user_management.views.invited_user.Mail")
    def test_succeeds(self, sendgrid_mock, mail_mock):
        # Make request as authenticated user with group
        data = {
            "email": "liltracy@gothboi.clique",
        }
        response = self.client.post("/invite-user/", data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(InvitedUser.objects.count(), 1)

        # Check that new invited user has the same group as user who made invite
        res_invited_user_id = response.data["id"]
        invited_user = InvitedUser.objects.get(id=res_invited_user_id)
        self.assertEqual(invited_user.company, self.company)
        # Check email sent
        self.assertEqual(sendgrid_mock.call_count, 1)

    def test_fails_no_token(self):
        no_auth_client = APIClient()
        # Make request as authenticated user with group
        data = {
            "email": "liltracy@gothboi.clique",
        }
        response = no_auth_client.post("/invite-user/", data, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class AcceptInviteTests(APITestCase):
    def test_succeeds(self):
        company = Company.objects.create(name="Kramerica Industries")
        invited_user = InvitedUser.objects.create(
            email="hewwo@uwu.tsss", company=company
        )
        # Make request as authenticated user with group
        data = {
            "id": invited_user.id,
            "password": "imgay",
            "username": "liltracy",
        }
        response = self.client.post("/accept-invite/", data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(UserSettings.objects.count(), 1)

        user_id = response.data["id"]
        # Check password correctly saved
        user = User.objects.get(id=user_id)
        self.assertIsNotNone(user.password)
        # Deletes invited user
        self.assertEqual(InvitedUser.objects.count(), 0)

    def test_fails_invalid_invited_user_sent(self):
        # When trying to sign up with non existing invited user id, it fails
        data = {
            "id": 100,
            "password": "imgay",
            "username": "liltracy",
        }
        response = self.client.post("/accept-invite/", data, format="json")
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
