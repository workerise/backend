from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from app.user_management.models import Company, InvitedUser, UserSettings
from django.contrib.auth.models import User

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient


class TestUsersViewset(APITestCase):
    def setUp(self):
        # Creates log in user
        self.user = User.objects.create(
            username="default", email="hewwo@hewwo.org", password="1234AbcD!"
        )
        self.company = Company.objects.create(name="Kramerica Industries")
        self.user_settings = UserSettings.objects.create(
            user=self.user, company=self.company
        )
        self.token = Token.objects.create(user=self.user)

        # Creates colleague
        self.colleague = User.objects.create(username="florentin", password="1234AbcD")
        self.colleague_user_settings = UserSettings.objects.create(
            user=self.colleague, company=self.company
        )

        # Creates other user with different company
        self.different_company = Company.objects.create(name="Google Inc.")
        self.stranger = User.objects.create(username="jorin", password="1234AbcD")
        self.stranger_user_settings = UserSettings.objects.create(
            user=self.stranger, company=self.different_company
        )

        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

    # List
    def test_succeeds_list_own_company(self):
        response = self.client.get("/users/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    # Detail
    def test_succeeds_detail_self(self):
        response = self.client.get("/users/{}/".format(self.user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_succeeds_detail_colleague(self):
        response = self.client.get("/users/{}/".format(self.colleague.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_fails_detail_stranger(self):
        response = self.client.get("/users/{}/".format(self.stranger.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    # Delete
    def test_succeeds_delete_self(self):
        response = self.client.delete("/users/{}/".format(self.user.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_fails_delete_colleague(self):
        response = self.client.delete("/users/{}/".format(self.colleague.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_fails_delete_stranger(self):
        response = self.client.delete("/users/{}/".format(self.stranger.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    # Edit
    def test_succeeds_edit_self(self):
        new_email = "newemail@email.com"
        response = self.client.patch(
            "/users/{}/".format(self.user.id), {"email": new_email}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_user = User.objects.get(id=self.user.id)
        self.assertEqual(new_email, updated_user.email)

    def test_fails_edit_colleague(self):
        new_email = "newemail@email.com"
        response = self.client.patch(
            "/users/{}/".format(self.colleague.id), {"email": new_email}
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_fails_edit_stranger(self):
        new_email = "newemail@email.com"
        response = self.client.patch(
            "/users/{}/".format(self.stranger.id), {"email": new_email}
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    # Create
    def test_fails_create(self):
        response = self.client.delete(
            "/users/", {"email": "hewwo@email.com", "username": "i'mgay"}
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class LoginTests(APITestCase):
    def setUp(self):
        self.user_data = {
            "username": "default",
            "password": "the_godfather",
            "email": "randallpark@gmail.com",
        }

        self.user = User.objects.create(
            username=self.user_data["username"],
            email=self.user_data["email"],
        )
        self.user.set_password(self.user_data["password"])
        self.user.save()

        self.company = Company.objects.create(name="Kramerica Industries")
        self.user_settings = UserSettings.objects.create(
            user=self.user, company=self.company
        )
        self.token = Token.objects.create(user=self.user)

    def test_succeeds(self):
        response = self.client.post(
            "/login/",
            {
                "username": self.user_data["username"],
                "password": self.user_data["password"],
            },
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["token"], self.token.key)

    def test_fails_invalid_credentials(self):
        response = self.client.post(
            "/login/",
            {"username": self.user_data["username"], "password": "wrongPassword"},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_fails_no_user(self):
        response = self.client.post(
            "/login/",
            {"username": "fake_username", "password": "wrongPassword"},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UserDataTests(APITestCase):
    def setUp(self):
        self.user_data = {
            "username": "default",
            "password": "the_godfather",
            "email": "randallpark@gmail.com",
        }

        self.user = User.objects.create(
            username=self.user_data["username"],
            email=self.user_data["email"],
        )
        self.user.set_password(self.user_data["password"])
        self.user.save()

        self.company = Company.objects.create(name="Kramerica Industries", number_of_employees=23)
        self.user_settings = UserSettings.objects.create(
            user=self.user, company=self.company
        )
        self.token = Token.objects.create(user=self.user)

        self.auth_client = APIClient()
        self.auth_client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

    def test_succeeds(self):
        response = self.auth_client.get(
            "/user-data/",
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["user"],
            {
                "id": self.user.id,
                "username": self.user.username,
                "email": self.user.email,
                "company": {
                    "id": self.company.id, 
                    "name": self.company.name, 
                    "number_of_employees": 23,
                    "number_of_works_council_members": 3,
                    },
            },
        )

    def test_fails_no_token(self):
        response = self.client.get(
            "/user-data/",
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_fails_wrong_token(self):
        wrong_client = APIClient()
        wrong_client.credentials(HTTP_AUTHORIZATION="Token " + "12345677")
        response = self.client.get(
            "/user-data/",
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ChangePasswordTestCase(APITestCase):
    def setUp(self):
        self.user_data = {
            "username": "default",
            "password": "rupaull_is_good_actually",
            "email": "randallpark@gmail.com",
        }

        self.user = User.objects.create(
            username=self.user_data["username"],
            email=self.user_data["email"],
        )
        self.user.set_password(self.user_data["password"])
        self.user.save()

        self.company = Company.objects.create(name="Kramerica Industries")
        self.user_settings = UserSettings.objects.create(
            user=self.user, company=self.company
        )
        self.token = Token.objects.create(user=self.user)

        self.auth_client = APIClient()
        self.auth_client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

    def test_succeeds(self):
        new_password = "miss_vanjie"

        response = self.auth_client.post(
            "/change-password/",
            {"old_password": self.user_data["password"], "new_password": new_password},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Attempt to login with new credentials
        client = APIClient()
        login_response = self.client.post(
            "/login/",
            {"username": self.user_data["username"], "password": new_password},
        )
        self.assertEqual(login_response.status_code, status.HTTP_200_OK)
        # Login fails with old credentials
        client = APIClient()
        login_response = self.client.post(
            "/login/",
            {
                "username": self.user_data["username"],
                "password": self.user_data["password"],
            },
        )
        self.assertEqual(login_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_fails_wrong_old_password(self):
        response = self.auth_client.post(
            "/change-password/",
            {"old_password": "miss_vanjie", "new_password": "cornerspaeticast"},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
