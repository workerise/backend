from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from app.timeline.models import Task
from app.timeline.config.tasks import tasks_definitions

from app.user_management.models import Company, InvitedUser, UserSettings
from django.contrib.auth.models import User

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient


class TestCompanyViewset(APITestCase):
    def setUp(self):
        # Creates log in user
        self.user = User.objects.create(username="default", password="1234AbcD!")
        self.company = Company.objects.create(name="Kramerica Industries")
        self.user_settings = UserSettings.objects.create(
            user=self.user, company=self.company
        )
        self.token = Token.objects.create(user=self.user)

        # Creates other user with different company
        self.different_company = Company.objects.create(name="Google Inc.")

        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

    def test_fails_list(self):
        response = self.client.get("/companies/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_fails_create(self):
        response = self.client.post("/companies/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_succeeds_detail_own_company(self):
        response = self.client.get("/companies/{}/".format(self.company.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_fails_detail_other_company(self):
        response = self.client.get("/companies/{}/".format(self.different_company.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_succeeds_update_name(self):
        new_name = "Paula Poundstone Industries"
        response = self.client.patch(
            "/companies/{}/".format(self.company.id),
            {"name": new_name},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_company = Company.objects.get(id=self.company.id)
        self.assertEqual(updated_company.name, new_name)

    def test_succeeds_update_number_of_employees(self):
        new_number_of_employees = 27
        response = self.client.patch(
            "/companies/{}/".format(self.company.id),
            {"number_of_employees": new_number_of_employees},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_company = Company.objects.get(id=self.company.id)
        self.assertEqual(updated_company.number_of_employees, new_number_of_employees)
        self.assertEqual(updated_company.get_number_of_works_council_members(), 3)

    def test_succeeds_update_name_and_number_of_employees(self):
        new_name = "Union Buster Busting Inc"
        new_number_of_employees = 42
        response = self.client.patch(
            "/companies/{}/".format(self.company.id),
            { "name": new_name, 
              "number_of_employees": new_number_of_employees },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_company = Company.objects.get(id=self.company.id)
        self.assertEqual(updated_company.name, new_name)
        self.assertEqual(updated_company.number_of_employees, new_number_of_employees)
        self.assertEqual(updated_company.get_number_of_works_council_members(), 3)

    def test_fails_delete(self):
        response = self.client.delete("/companies/{}/".format(self.company.id))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class InitializeCompanyTests(APITestCase):
    def test_succeeds(self):
        data = {
            "company_name": "Kamerica Industries",
            "company_number_of_employees": 123,
            "username": "lilpeep",
            "password": "lenin_rulez",
            "email": "example@email.com",
        }
        response = self.client.post("/initialize-company/", data, format="json")

        # Response code
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Objects created
        self.assertEqual(Company.objects.count(), 1)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(UserSettings.objects.count(), 1)

        # Check user has correct username and email
        res_user_id = response.data["user"]["id"]
        user = User.objects.get(id=res_user_id)
        self.assertEqual(user.username, "lilpeep")
        self.assertEqual(user.email, "example@email.com")

        # check company has correct name, number of employees and number of works council members
        user_settings = UserSettings.objects.get(user=user)
        company = Company.objects.get(id=user_settings.company.id)
        self.assertEqual(company.name, "Kamerica Industries")
        self.assertEqual(company.number_of_employees, 123)
        self.assertEqual(company.get_number_of_works_council_members(), 7)
        
        # Check tasks were made
        tasks = Task.objects.filter(company=company)
        self.assertEqual(len(tasks), len(tasks_definitions))

    def test_succeeds_without_number_of_employees(self):
        data = {
            "company_name": "Kamerica Industries",
            "username": "bigpeep",
            "password": "lenin_rulez",
            "email": "example2@email.com",
        }
        response = self.client.post("/initialize-company/", data, format="json")

        # Response code
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Objects created
        self.assertEqual(Company.objects.count(), 1)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(UserSettings.objects.count(), 1)

        # Check user has correct username and email
        res_user_id = response.data["user"]["id"]
        user = User.objects.get(id=res_user_id)
        self.assertEqual(user.username, "bigpeep")
        self.assertEqual(user.email, "example2@email.com")

        # check company has correct name, and number of employees and number of works council members are not set
        user_settings = UserSettings.objects.get(user=user)
        company = Company.objects.get(id=user_settings.company.id)
        self.assertEqual(company.name, "Kamerica Industries")
        self.assertEqual(company.number_of_employees, None)
        self.assertEqual(company.get_number_of_works_council_members(), None)
        
        # Check tasks were made
        tasks = Task.objects.filter(company=company)
        self.assertEqual(len(tasks), len(tasks_definitions))

    def test_fails_missing_fields(self):
        data = {
            "username": "lilpeep",
            "password": "lenin_rulez",
            "email": "example@email.com",
        }
        response = self.client.post("/initialize-company/", data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_fails_user_with_username_exists(self):
        email = "example@email.com"
        username = "yunglean"
        User.objects.create(username=username, password="serenity now!", email=email)
        data = {
            "company_name": "Kamerica Industries",
            "username": username,
            "password": "lenin_rulez",
            "email": email,
        }
        response = self.client.post("/initialize-company/", data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
