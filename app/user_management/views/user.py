from django.contrib.auth import authenticate, hashers
from django.contrib.auth.models import User

from rest_framework import viewsets, permissions, authentication, status, mixins
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)

from app.user_management.models import Company, UserSettings
from app.user_management.serializers import (
    UserSerializer,
    UserSettingsSerializer,
)


class OnlyEditSelf(permissions.BasePermission):
    """
    Only allows a user to Edit or Delete their own user model.
    This permission is only for the User Model.
    """

    def has_object_permission(self, request, view, obj):
        if request.method in ["DELETE", "PUT", "PATCH"]:
            return request.user == obj
        return True


class UserViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    """
    User can retrieve a list of users or any user from their own company.
    They can also edit or delete themself.

    User models are only created with the endpoints `/initialize-company/`
    or `/accept-invite/`
    """

    serializer_class = UserSerializer
    authentication_classes = [authentication.TokenAuthentication]
    # Can only edit own user
    permission_classes = [permissions.IsAuthenticated, OnlyEditSelf]

    def get_queryset(self):
        # Only return users with own company
        req_user_settings = UserSettings.objects.get(user=self.request.user)
        req_user_company = req_user_settings.company
        colleagues_user_settings = UserSettings.objects.filter(
            company__id=req_user_company.id
        )
        colleagues = User.objects.filter(
            id__in=[settings.user.id for settings in colleagues_user_settings]
        )
        return colleagues


@api_view(["POST"])
def login(request):
    # Get user from username
    user = authenticate(
        username=request.data.get("username"), password=request.data.get("password")
    )
    # If authenticated
    if user is not None:
        # Get userdata
        user_settings = UserSettings.objects.get(user=user)
        # Make token
        token, created = Token.objects.get_or_create(user=user)
        return Response(
            {
                "token": token.key,
            },
            status=status.HTTP_200_OK,
        )
    # If not authenticated
    else:
        return Response(
            data={"no_detail": "Credentials not valid"},
            status=status.HTTP_400_BAD_REQUEST,
        )


@api_view(["GET"])
@authentication_classes([authentication.TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def user_data(request):
    """
    Get user data for the user associated with the token
    """
    user = request.user
    user_settings = UserSettings.objects.get(user=user)
    return Response(
        {
            "user": {
                "id": user.id,
                "username": user.get_username(),
                "email": user.email,
                "company": {
                    "id": user_settings.company.id,
                    "name": user_settings.company.name,
                    "number_of_employees": user_settings.company.number_of_employees,
                    "number_of_works_council_members": user_settings.company.get_number_of_works_council_members()
                },
            },
        },
        status=status.HTTP_200_OK,
    )


@api_view(["POST"])
@authentication_classes([authentication.TokenAuthentication])
@permission_classes([permissions.IsAuthenticated])
def change_password(request):
    """
    Changes the user's password
    """
    user = request.user

    correct_password = hashers.check_password(
        request.data.get("old_password"), user.password
    )

    if correct_password:
        new_password = request.data.get("new_password")
        user.set_password(new_password)
        user.save()

        return Response(
            status=status.HTTP_204_NO_CONTENT,
        )
    # If not authenticated
    else:
        return Response(
            data={"no_detail": "Credentials not valid"},
            status=status.HTTP_403_FORBIDDEN,
        )
