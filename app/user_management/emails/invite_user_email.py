def build_invite_user_email(invited_user_id):
    return """
            <h2>You've been invited by a colleague to use Workerise.</h2>
            <p>
                <a href='https://workerise.org/accept-invite?id={}'>
                    Click here to accept
                </a>
            </p>
            <br>
            <br>
            <h2>Sie sind von einem Kollegen eingeladen worden, Workerise zu verwenden.</h2>
            <p>
                <a href='https://workerise.org/accept-invite?id={}'>
                    Hier klicken, um zu akeptieren.
                </a>
            </p>
            """.format(
        invited_user_id, invited_user_id
    )
