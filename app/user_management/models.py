from __future__ import unicode_literals
import uuid
from django.contrib.auth.models import User
from django.db import models
from app.timeline.config.works_council_size_map import WorksCouncilSizeMap

class Company(models.Model):
    name = models.CharField(unique=True, max_length=80)
    number_of_employees = models.IntegerField(blank=True, null=True)

    def get_number_of_works_council_members(self):
        map = WorksCouncilSizeMap()
        return map.get_number_of_works_council_members(self.number_of_employees)


class InvitedUser(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField()
    company = models.ForeignKey(Company, on_delete=models.CASCADE)


class UserSettings(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
