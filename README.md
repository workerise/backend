# Workerise MVP Backend

## Frameworks

- Django
- Django Rest Framework

## Running development

### System Dependencies

- python3
- pip3
- virtualenv

### Set up

When first cloning the project, to set up run:

```bash
# Create a virtual env
python -m venv venv
# Activate it
source venv/bin/activate
# Install requirements
pip install -r requirements.txt
# Create a local database with correct schemas
python manage.py migrate
```

### Running in development

```bash
python manage.py runserver
```

### Tests

To run tests:

```bash
python manage.py test
```

When writing API unit tests, please follow this naming convention:

```python
# test style:
#   test_succeeds_list_own_company
#   test_succeeds_list_other_company
#   test_fails_delete_stranger
#   test_fails_delete_self
#   test_fails_delete_colleague
```

The convention is: `test_<succeeds|fails>_<api-action>_<relationship-to-user>_<model?>`

## Email

Emails are sent using Sendgrid using their [python library](https://www.google.com/search?channel=fs&client=ubuntu&q=sendgrid+python). The `.env` variable for the SendGrid API key is `SENDGRID_API_KEY`.

### Testing

Mock the SendgridClient and Mail classes like this:

```python
@mock.patch("app.user_management.views.invited_user.SendGridAPIClient.send")
@mock.patch("app.user_management.views.invited_user.Mail")
def test_view_with_email(self, mock_sendgrid, mock_mail):
    # Send request to view
    self.assertEqual(mock_sendgrid.call_count, 1)
```
